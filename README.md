Run e2e tests
```
> yarn e2e
```

Run unit tests
```
> yarn test
```

Generage coverage report after unit tests run
```
> yarn coverage
```

Run linter with autofix + prettier
```
> yarn lint
```

Run using iphoneX. Use two different terminal windows.
```
terminal1> yarn start
terminal2> yarn ios:X
```

Assemble beta-build for testing(iOS)
CAUTION: SAMPLE ONLY! - not working actually => need certificates and etc...
```
> yarn beta:ios
```

