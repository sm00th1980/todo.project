import LUniqueId from 'lodash.uniqueid';
import { reject, isNil } from 'ramda';

export const noop = () => {};

export const uniqId = LUniqueId;

export const compact = items => {
  if (isNil(items)) {
    return [];
  }

  return reject(item => isNil(item), items);
};
