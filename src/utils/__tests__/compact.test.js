import { compact } from '..';

describe('compact', () => {
  it('reject nil values into array', () => {
    const input = [1, null, undefined, 4];
    expect(compact(input)).toEqual([1, 4]);
  });

  it('return empty array if input is nil', () => {
    const input = undefined;
    expect(compact(input)).toEqual([]);
  });
});
