import {
  FINISH_TODO,
  ADD_TODO,
  CREATE_TODO,
  DROP_TODO,
} from '../constants/actions';

export const finishTodo = payload => ({
  type: FINISH_TODO,
  payload,
});

export const addTodo = () => ({
  type: ADD_TODO,
});

export const createTodo = payload => ({
  type: CREATE_TODO,
  payload,
});

export const dropTodo = payload => ({
  type: DROP_TODO,
  payload,
});
