import { uniqId } from '../utils';

import {
  FINISH_TODO,
  ADD_TODO,
  CREATE_TODO,
  DROP_TODO,
} from '../constants/actions';
import { finishTodo, addTodo, createTodo, dropTodo } from './utils';

const initialState = {
  todos: [
    { key: uniqId(), title: 'First task', finished: false },
    { key: uniqId(), title: 'Second task', finished: false },
    { key: uniqId(), title: 'Third task', finished: false },
    { key: uniqId(), title: 'Forth task', finished: false },
    { key: uniqId(), title: 'Finished task', finished: true },
  ],
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case FINISH_TODO:
      return {
        ...state,
        todos: finishTodo(state.todos, payload.todoKey),
      };

    case ADD_TODO:
      return {
        ...state,
        todos: addTodo(state.todos),
      };

    case CREATE_TODO:
      return {
        ...state,
        todos: createTodo(state.todos, payload.todoTitle),
      };

    case DROP_TODO:
      return {
        ...state,
        todos: dropTodo(state.todos, payload.todoKey),
      };

    default:
      return state;
  }
}
