import { reject } from 'ramda';
import { uniqId } from '../../utils';

export const finishTodo = (todos, todoKey) =>
  todos.map(todo => {
    if (todo.key === todoKey) {
      return { ...todo, finished: true };
    }

    return todo;
  });

export const addTodo = todos => [
  {
    input: true,
    finished: false,
    key: uniqId(),
    title: '',
  },
  ...todos,
];

export const createTodo = (todos, todoTitle) => {
  const todosWithoutInput = reject(todo => todo.input, todos);

  return [
    {
      finished: false,
      key: uniqId(),
      title: todoTitle,
    },
    ...todosWithoutInput,
  ];
};

export const dropTodo = (todos, todoKey) =>
  reject(todo => todo.key === todoKey, todos);
