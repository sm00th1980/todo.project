import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducers from '../reducers';

const middleware = [thunk];
const enhancers = [];

const configureStore = preloadedState =>
  createStore(
    reducers,
    preloadedState,
    compose(
      applyMiddleware(...middleware),
      ...enhancers
    )
  );

export default configureStore;
