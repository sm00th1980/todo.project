import React from 'react';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import Todos from './components/screens/todos';

const store = configureStore({});

const App = () => (
  <Provider store={store}>
    <Todos />
  </Provider>
);

export default App;
