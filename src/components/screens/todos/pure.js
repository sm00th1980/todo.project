import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';
import { head, find } from 'ramda';
import Header from './Header';
import Select from './Select';
import Table from './Table';
import { PADDING } from './config';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    flex: 1,
    paddingRight: PADDING,
  },
});

const TABS = [
  {
    title: 'Todo',
    key: 0,
    internalKey: 'TODOS',
  },
  {
    title: 'All',
    key: 1,
    internalKey: 'ALL_TODOS',
  },
];

const isAllTab = tab => tab.internalKey === 'ALL_TODOS';
const todoTab = tabs => find(tab => tab.internalKey === 'TODOS', tabs);

class Todo extends Component {
  state = {
    activeTab: head(TABS),
  };

  handleSelectTab = tab => {
    this.setState({
      activeTab: tab,
    });
  };

  handleAddTodo = () => {
    this.setState(
      {
        activeTab: todoTab(TABS),
      },
      () => {
        const { addTodo } = this.props;
        addTodo();
      }
    );
  };

  handleSubmit = text => {
    const { createTodo } = this.props;
    createTodo(text);
  };

  render() {
    const { items, finishTodo, notFinishedItems, dropTodo } = this.props;
    const { activeTab } = this.state;

    return (
      <View testID="todo" style={styles.container}>
        <Header title="TODO'S" onAdd={this.handleAddTodo} />
        <View style={styles.contentContainer}>
          <Select
            tabs={TABS}
            onChoose={this.handleSelectTab}
            activeTabKey={activeTab.key}
          />
          {isAllTab(activeTab) && (
            <Table items={items} finishTodo={dropTodo} actionTitle="Drop" />
          )}
          {!isAllTab(activeTab) && (
            <Table
              items={notFinishedItems}
              finishTodo={finishTodo}
              onSubmit={this.handleSubmit}
              actionTitle="Finish"
            />
          )}
        </View>
      </View>
    );
  }
}

Todo.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      input: PropTypes.bool,
    })
  ).isRequired,
  notFinishedItems: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      input: PropTypes.bool,
    })
  ).isRequired,
  finishTodo: PropTypes.func.isRequired,
  addTodo: PropTypes.func.isRequired,
  createTodo: PropTypes.func.isRequired,
  dropTodo: PropTypes.func.isRequired,
};

export default Todo;
