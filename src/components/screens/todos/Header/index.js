import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { noop } from '../../../../utils';

import { ACTION_WIDTH, HEIGHT, BACKGROUND_COLOR } from './config';

const styles = StyleSheet.create({
  container: {
    height: HEIGHT,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: BACKGROUND_COLOR,
  },
  titleContainer: {
    paddingLeft: ACTION_WIDTH,
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: 10,
  },
  title: {
    color: '#484A54',
    fontSize: 16,
    fontFamily: 'OpenSans-SemiBold',
    textAlign: 'center',
  },
  actionContainer: {
    width: ACTION_WIDTH,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: 5,
  },
});

const Header = ({ title, onAdd }) => (
  <View style={styles.container}>
    <View style={styles.titleContainer}>
      <Text style={styles.title}>{title}</Text>
    </View>
    <TouchableOpacity
      style={styles.actionContainer}
      onPress={onAdd}
      testID="add_task_button"
    >
      <Icon name="plus-circle" size={30} color="#484A54" />
    </TouchableOpacity>
  </View>
);

Header.propTypes = {
  title: PropTypes.string.isRequired,
  onAdd: PropTypes.func,
};

Header.defaultProps = {
  onAdd: noop,
};

export default Header;
