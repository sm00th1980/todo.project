/* eslint import/prefer-default-export: off */
import { partial } from 'ramda';

export const toData = (item, onOpen, onDrop, onSubmit) => ({
  ...item,
  onOpen: partial(onOpen, [item.key]),
  onDrop: partial(onDrop, [item.key]),
  onSubmit,
});
