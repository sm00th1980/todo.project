import React from 'react';
import { View, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: 1,
    backgroundColor: 'white',
  },
  item: {
    flex: 1,
    backgroundColor: '#F5F4F8',
  },
});

const ItemSeparator = () => (
  <View style={styles.container}>
    <View style={styles.item} />
  </View>
);

export default ItemSeparator;
