import React, { Component } from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
import PropTypes from 'prop-types';
import { isEmpty, isNil } from 'ramda';
import ItemSeparator from './ItemSeparator';
import ListEmpty from './ListEmpty';
import TableCellWithDrop from './TableCellWithDrop';
import { toData } from './utils';
import { noop } from '../../../../utils';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

class Table extends Component {
  constructor(props) {
    super(props);

    this.rows = [];
  }

  handleOpen = key => {
    this.rows.forEach(({ rowRef, rowKey }) => {
      if (rowKey !== key && !isNil(rowRef)) {
        // close opened cell
        rowRef.close();
      }
    });
  };

  handleDrop = todoKey => {
    const { finishTodo } = this.props;
    finishTodo(todoKey);
  };

  render() {
    const { items, onSubmit, actionTitle } = this.props;

    if (isEmpty(items)) {
      return <ListEmpty />;
    }

    return (
      <View style={styles.container}>
        <FlatList
          data={items}
          renderItem={({ item }) => (
            <TableCellWithDrop
              {...toData(item, this.handleOpen, this.handleDrop, onSubmit)}
              onRef={cell => this.rows.push({ rowRef: cell, rowKey: item.key })}
              actionTitle={actionTitle}
            />
          )}
          ItemSeparatorComponent={ItemSeparator}
        />
      </View>
    );
  }
}

Table.propTypes = {
  actionTitle: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      input: PropTypes.bool,
    })
  ).isRequired,
  finishTodo: PropTypes.func.isRequired,
  onSubmit: PropTypes.func,
};

Table.defaultProps = {
  onSubmit: noop,
};

export default Table;
