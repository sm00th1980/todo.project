/* eslint no-underscore-dangle: off */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  View,
  Animated,
  TouchableOpacity,
  PanResponder,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { PADDING } from '../../../config';
import { noop } from '../../../../../../utils';

const HEIGHT = 60;
const FONT_SIZE = 17;

const IMAGE_SIZE = HEIGHT * 0.65;

const START_OFFSET_X = 0;
const END_OFFSET_X = 100;

export const styles = StyleSheet.create({
  container: {
    height: HEIGHT,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  animateContainer: {
    position: 'absolute',
    top: 0,
    left: PADDING,
    right: 0,
    bottom: 0,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  title: {
    fontSize: FONT_SIZE,
  },
  imageContainer: {
    justifyContent: 'center',
    paddingRight: 10,
  },
  image: {
    height: IMAGE_SIZE,
    aspectRatio: 1,
    backgroundColor: '#ECF1FB',
    borderRadius: IMAGE_SIZE,
    justifyContent: 'center',
    alignItems: 'center',
  },
  deleteContainer: {
    width: END_OFFSET_X - START_OFFSET_X,
    height: HEIGHT,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red',
  },
  delete: {
    fontSize: FONT_SIZE,
    color: 'white',
  },
  titleContainer: {
    flex: 1,
    height: HEIGHT,
    alignItems: 'center',
    flexDirection: 'row',
  },
});

class DropableCell extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offsetX: new Animated.Value(0), // inits to zero
    };

    this.state.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: this.onPanResponderMove,
      onPanResponderRelease: this.onPanResponderRelease,
    });

    this.opened = false;
    this.initialOffsetX = 0;
    this.isToggling = false;
  }

  onPanResponderMove = (evt, gestureState) => {
    if (!this.isToggling) {
      const { dx } = gestureState;
      const { offsetX } = this.state;

      offsetX.setValue(this.initialOffsetX - dx);

      const isOpening = dx < 0;

      const toggle = Math.abs(dx) >= Math.abs(END_OFFSET_X) * 0.3;

      if (toggle && isOpening && !this.opened) {
        this.open();
      }

      if (toggle && !isOpening && this.opened) {
        this.close();
      }
    }
  };

  onPanResponderRelease = () => {
    if (!this.isToggling) {
      const { offsetX } = this.state;
      const currentOffsetX = offsetX.__getValue();

      if (START_OFFSET_X < currentOffsetX && currentOffsetX < END_OFFSET_X) {
        if (this.opened) {
          this.open();
        } else {
          this.close();
        }
      }
    }
  };

  open() {
    this.isToggling = true;
    const { offsetX } = this.state;

    Animated.timing(offsetX, {
      toValue: END_OFFSET_X,
      duration: 100,
    }).start(() => {
      this.initialOffsetX = END_OFFSET_X;
      this.opened = true;
      this.isToggling = false;

      const { onOpen } = this.props;
      onOpen();
    });
  }

  close() {
    this.isToggling = true;
    const { offsetX } = this.state;

    Animated.timing(offsetX, {
      toValue: START_OFFSET_X,
      duration: 100,
    }).start(() => {
      this.initialOffsetX = START_OFFSET_X;
      this.opened = false;
      this.isToggling = false;

      const { onClose } = this.props;
      onClose();
    });
  }

  render() {
    const {
      offsetX,
      panResponder: { panHandlers },
    } = this.state;
    const { title, onDrop, actionTitle } = this.props;

    const translateX = Animated.multiply(
      offsetX.interpolate({
        inputRange: [START_OFFSET_X, END_OFFSET_X],
        outputRange: [START_OFFSET_X, END_OFFSET_X],
        extrapolate: 'clamp',
      }),
      -1
    );

    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.deleteContainer}
          activeOpacity={0.5}
          onPress={onDrop}
        >
          <Text style={styles.delete}>{actionTitle}</Text>
        </TouchableOpacity>

        <Animated.View
          {...panHandlers}
          style={{
            ...styles.animateContainer,
            transform: [{ translateX }],
          }}
        >
          <View style={styles.imageContainer}>
            <View style={styles.image}>
              <Icon name="image" size={15} color="#97AFF3" />
            </View>
          </View>
          <View style={styles.titleContainer}>
            <Text style={styles.title} numberOfLines={1}>
              {title}
            </Text>
          </View>
        </Animated.View>
      </View>
    );
  }
}

DropableCell.propTypes = {
  title: PropTypes.string.isRequired,
  actionTitle: PropTypes.string.isRequired,
  onDrop: PropTypes.func,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
};

DropableCell.defaultProps = {
  onDrop: noop,
  onOpen: noop,
  onClose: noop,
};

export default DropableCell;
