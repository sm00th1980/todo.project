import React from 'react';
import { shallow } from 'enzyme';
import DropableCell from '..';

describe('DropableCell', () => {
  it('render without errors', () => {
    expect(() => {
      const props = {
        title: 'title',
        actionTitle: 'actionTitle',
      };

      const component = shallow(<DropableCell {...props} />);
      expect(component).toBeTruthy();
    }).not.toThrow();
  });

  it('handle drop', () => {
    const props = {
      title: 'title',
      onDrop: jest.fn(),
      actionTitle: 'actionTitle',
    };

    const cell = shallow(<DropableCell {...props} />);
    const dropBtn = cell.find('TouchableOpacity');

    const { onDrop } = props;
    expect(onDrop).not.toHaveBeenCalled();
    dropBtn.simulate('press');
    expect(onDrop).toHaveBeenCalled();
  });
});
