/* eslint no-return-assign: off */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TextInput, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { PADDING } from '../../../config';

const HEIGHT = 60;
const FONT_SIZE = 17;

const IMAGE_SIZE = HEIGHT * 0.65;

export const styles = StyleSheet.create({
  container: {
    height: HEIGHT,
    backgroundColor: 'white',
    paddingLeft: PADDING,
    paddingRight: PADDING + IMAGE_SIZE,
    alignItems: 'center',
    flexDirection: 'row',
  },
  image: {
    height: IMAGE_SIZE,
    aspectRatio: 1,
    backgroundColor: '#ECF1FB',
    borderRadius: IMAGE_SIZE,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
  },
  input: {
    fontSize: FONT_SIZE,
    fontFamily: 'OpenSans-Regular',
  },
});

class Input extends Component {
  state = {
    value: '',
  };

  componentDidMount() {
    if (this.input) {
      this.input.focus();
    }
  }

  handlePress = () => {
    if (this.input) {
      this.input.focus();
    }
  };

  handleSubmit = () => {
    const { onSubmit } = this.props;
    const { value } = this.state;

    onSubmit(value);
  };

  handleChangeText = value => {
    this.setState({ value });
  };

  render() {
    const { value } = this.state;

    return (
      <TouchableOpacity
        style={styles.container}
        onPress={this.handlePress}
        activeOpacity={1}
        testID="new_task_input"
      >
        <View style={styles.image}>
          <Icon name="image" size={15} color="#97AFF3" />
        </View>
        <TextInput
          ref={c => (this.input = c)}
          keyboardType="email-address"
          style={styles.input}
          placeholder="New todo"
          placeholderTextColor="#ADACB4"
          autoCorrect={false}
          onSubmitEditing={this.handleSubmit}
          onChangeText={this.handleChangeText}
          value={value}
        />
      </TouchableOpacity>
    );
  }
}

Input.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default Input;
