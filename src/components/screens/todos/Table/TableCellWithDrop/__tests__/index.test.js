import React from 'react';
import { shallow } from 'enzyme';
import TableCellWithDrop from '..';

describe('TableCellWithDrop', () => {
  it('render without errors', () => {
    expect(() => {
      const props = {
        title: 'title',
        actionTitle: 'actionTitle',
      };

      const component = shallow(<TableCellWithDrop {...props} />);
      expect(component).toBeTruthy();
    }).not.toThrow();
  });
});
