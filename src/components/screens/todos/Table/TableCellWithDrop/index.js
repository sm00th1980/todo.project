import React from 'react';
import PropTypes from 'prop-types';

import { noop } from '../../../../../utils';

import DropableCell from './DropableCell';
import Input from './Input';

const TableCellWithDrop = ({
  title,
  onDrop,
  input,
  onOpen,
  onClose,
  onRef,
  onSubmit,
  actionTitle,
}) => {
  if (input) {
    return <Input onSubmit={onSubmit} />;
  }

  return (
    <DropableCell
      ref={cell => onRef(cell)}
      title={title}
      onDrop={onDrop}
      onOpen={onOpen}
      onClose={onClose}
      actionTitle={actionTitle}
    />
  );
};

TableCellWithDrop.propTypes = {
  title: PropTypes.string.isRequired,
  actionTitle: PropTypes.string.isRequired,
  onDrop: PropTypes.func,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
  onRef: PropTypes.func,
  onSubmit: PropTypes.func,
  input: PropTypes.bool,
};

TableCellWithDrop.defaultProps = {
  onDrop: noop,
  onOpen: noop,
  onClose: noop,
  onRef: noop,
  onSubmit: noop,
  input: false,
};

export default TableCellWithDrop;
