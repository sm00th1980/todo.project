import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: '#484A54',
    fontSize: 23,
    textAlign: 'center',
    fontFamily: 'OpenSans-SemiBold',
    marginBottom: 10,
  },
  description: {
    color: '#484A54',
    fontSize: 15,
    fontFamily: 'OpenSans-Regular',
    textAlign: 'center',
  },
});

const ListEmpty = () => (
  <View style={styles.container}>
    <Text style={styles.title}>Sorry</Text>
    <Text style={styles.description}>No any todo&apos;s yet.</Text>
  </View>
);

export default ListEmpty;
