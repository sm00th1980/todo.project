import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  finishTodo as finishTodoAC,
  addTodo as addTodoAC,
  createTodo as createTodoAC,
  dropTodo as dropTodoAC,
} from '../../../AC/todo';

import { getAllTodos, getNotFinishedTodos } from '../../../selectors';

import Todos from './pure';

const mapStateToProps = state => ({
  items: getAllTodos(state),
  notFinishedItems: getNotFinishedTodos(state),
});

const mapDispatchToProps = dispatch => ({
  finishTodo: bindActionCreators(finishTodoAC, dispatch),
  addTodo: bindActionCreators(addTodoAC, dispatch),
  createTodo: bindActionCreators(createTodoAC, dispatch),
  dropTodo: bindActionCreators(dropTodoAC, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Todos);
