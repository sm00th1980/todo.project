import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { noop } from '../../../../../utils';

const RADIUS = 2;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  textActive: {
    color: '#97AFF3',
    fontSize: 15,
    fontFamily: 'OpenSans-Regular',
    textAlign: 'center',
    marginBottom: 5,
  },
  textNotActive: {
    color: '#97AFF3',
    fontSize: 15,
    fontFamily: 'OpenSans-Regular',
    textAlign: 'center',
    marginBottom: 7,
  },
  bottomSeparator: {
    marginLeft: 50,
    marginRight: 50,
    height: RADIUS,
    backgroundColor: '#97AFF3',
    borderTopLeftRadius: RADIUS,
    borderTopRightRadius: RADIUS,
  },
});

const Tab = ({ title, active, index, onPress }) => (
  <TouchableOpacity
    style={styles.container}
    onPress={() => onPress(index)}
    testID={`${title}_button`}
  >
    <Text
      style={active ? styles.textActive : styles.textNotActive}
      numberOfLines={1}
    >
      {title}
    </Text>
    {active && <View style={styles.bottomSeparator} />}
  </TouchableOpacity>
);

Tab.propTypes = {
  title: PropTypes.string.isRequired,
  active: PropTypes.bool,
  index: PropTypes.number.isRequired,
  onPress: PropTypes.func,
};

Tab.defaultProps = {
  active: false,
  onPress: noop,
};

export default Tab;
