import React from 'react';
import { StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';
import Tab from './Tab';
import { noop } from '../../../../utils';

const styles = StyleSheet.create({
  container: {
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: '#F5F4F8',
    flexDirection: 'row',
  },
});

const Select = ({ tabs, onChoose, activeTabKey }) => (
  <View style={styles.container}>
    {tabs.map(tab => (
      <Tab
        title={tab.title}
        active={tab.key === activeTabKey}
        onPress={() => onChoose(tab)}
        key={tab.key}
        index={tab.key}
      />
    ))}
  </View>
);

Select.propTypes = {
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
    })
  ).isRequired,
  activeTabKey: PropTypes.number.isRequired,
  onChoose: PropTypes.func,
};

Select.defaultProps = {
  onChoose: noop,
};

export default Select;
