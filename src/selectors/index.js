import { pathOr, reject } from 'ramda';
import { compact } from '../utils';

export const getAllTodos = state => pathOr([], ['todos', 'todos'], state);
export const getNotFinishedTodos = state => {
  const todos = pathOr([], ['todos', 'todos'], state);
  return reject(todo => todo.finished, compact(todos));
};
