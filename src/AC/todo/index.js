/* eslint import/prefer-default-export: off */

import {
  finishTodo as finishTodoAction,
  addTodo as addTodoAction,
  createTodo as createTodoAction,
  dropTodo as dropTodoAction,
} from '../../actions';

export const finishTodo = todoKey => dispatch => {
  dispatch(
    finishTodoAction({
      todoKey,
    })
  );
};

export const addTodo = () => dispatch => {
  dispatch(addTodoAction());
};

export const createTodo = todoTitle => dispatch => {
  dispatch(
    createTodoAction({
      todoTitle,
    })
  );
};

export const dropTodo = todoKey => dispatch => {
  dispatch(
    dropTodoAction({
      todoKey,
    })
  );
};
