import { finishTodo } from '..';
import { FINISH_TODO } from '../../../constants/actions';

describe('finishTodo()', () => {
  it('finish todo', () => {
    expect.assertions(1);

    const todoKey = 'todoKey';
    const mockDispatch = jest.fn();

    finishTodo(todoKey)(mockDispatch);

    expect(mockDispatch.mock.calls).toEqual([
      [
        {
          type: FINISH_TODO,
          payload: { todoKey },
        },
      ],
    ]);
  });
});
