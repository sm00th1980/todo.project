describe('Example', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('should have todo screen', async () => {
    await expect(element(by.id('todo'))).toBeVisible();
  });

  it('should show finished tasks after switch to all tasks', async () => {
    await expect(element(by.text('Finished task'))).toBeNotVisible();
    await element(by.id('All_button')).tap();
    await expect(element(by.text('Finished task'))).toBeVisible();
  });

  it('should show input for new task after tap on add_new_task_button', async () => {
    await expect(element(by.id('new_task_input'))).toBeNotVisible();
    await element(by.id('add_task_button')).tap();
    await expect(element(by.id('new_task_input'))).toBeVisible();
  });
});
